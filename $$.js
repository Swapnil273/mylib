function $$(data){
Array.prototype.forEach2=function(callback){
    var l=this.length;
    for(let i=0;i<l;i++)callback(this[i],i);
};
//********TYPE CONSTRUCTOR DEFINITION *******
$$.Types=function(types,ands,nots,ors,props,force,useInstOf){
    this.typeConstructors=types;
    this.nots=nots;
    this.ands=ands;
    this.ors=ors;
    this.props=props;
    this.force=force;
    this.useInstanceOf=useInstOf;
};
var Types=$$.Types;
//Predefined values
Types.predef={
        "*":{
            nots:{
                "falsly":function(val){return !!val},
                "nullish":function(val){return val!==null&&val!==undefined},
                "undefined":function(val){return val!==undefined}
            }
        },
        "Number":{
            nots:{"NaN":function(val){return val===val}},
            ands:{"Int":function(val){return !(val%1)}}
        },
        "Array":{
            props:{
                "*":function(val,type){return val.every(x=>Types.checkType(x,type))}
            },
            nots:{"empty":function(val){return val.length!==0}},
            ands:{}
        }
};
//Types.operators=["!","&","|"];
//********* TYPE PARSING *********
/*
constructor&ands!nots[propName:propType prop2:propType2]|alternate type|.....
@I-> Use instance of
@Iconstructor
| defines alternate types any one of the types must be matched.
&ands and !nots are taken from Types.predef and define additional functionality.
* means anything in constructor
In props, * is a special property meant for iterating over the object.
props are seperated by spaces and are defined by colons.
You can use $$(function).locktype({argument:Array of types for each argument of function, returnValue: the type for return value}) which returns a new function which has types locked.
To be added :
    @F for forced conversion instead of giving errors.
    TypeLocking for Arrays
    TypeLocking for slices of arguments: eg certain type for all function arguments.
*/
Types.parse=function(tdata){
   // console.log(JSON.stringify(data))
    var force=tdata.match(/^@F/);
    if(force){
        tdata=tdata.slice(2);
    };
    var constructors=tdata.match(/\|/)?tdata.split("|"):[tdata];
    var parsed=null;
    var first=null;
    constructors.forEach2((x,y)=>{
        var useInstOf=x.match(/^@I/);
        if(useInstOf){
            x=x.slice(2);
        }
        var realConstructor=x.match(/^[A-Z0-9*_]+/i).toString();
        var constructorString=x.match(/^[^\[\]]+/)[0];
        var nots=constructorString.match(/[a-z0-9_*]\![^&!\[\]]+/ig);
        nots&&(nots=nots.map(m=>m.slice(2)));
        var ands=constructorString.match(/[a-z0-9_*]\&[^!&\[\]]+/ig)
        ands&&(ands=ands.map(m=>m.slice(2)));
        //var stack=[];
        var props= x.match(/\[[\s\S]*\]/);
        var parsedProps=null;
        if(props){
            parsedProps={};
            props.toString().slice(1,-1).split(" ").forEach(p=>{
                var property=p.split(":");
                property=[property[0],property.slice(1).join(":")];
                parsedProps[property[0]]=Types.parse(property[1]);
            });
        }
        (y!=0&&!parsed&&(parsed=[]));
        if(y!=0)parsed.push(new Types(realConstructor,ands,nots,null,parsedProps,first.force,!!useInstOf ));
        else first=new Types(realConstructor,ands,nots,parsed,parsedProps,!!force,!!useInstOf);
    });
    return first;
}


//********TYPE CHECKING *********
Types.checkType=function(val,type){
   // console.log(""+val+JSON.stringify(type));
        if(type.ors)for(let x of type.ors){
            if(Types.checkType(val,x))return true;
        }
        var checksDone={
            ands:false,
            nots:false,
            constructors:false,
            props:false,
            get all(){
                return this.ands && this.nots && this.constructors && this.props;
            }
        };
        typeConstructorBlock:{
            if(type.typeConstructors==="*"){
                checksDone.constructors=true;
                //return false;
                break typeConstructorBlock;
            }
            if(val===undefined){
                checksDone.constructors=type.typeConstructors==="undefined";
                break typeConstructorBlock;
            }
            if(val===null){
                checksDone.constructors=type.typeConstructors==="null";
                break typeConstructorBlock;
            }
            if(type.useInstanceOf){
                checksDone.constructors=val instanceof window[type.typeConstructors];
                
            }else{
                checksDone.constructors=val.constructor.name==type.typeConstructors ;
            }
        };
        if(!checksDone.constructors)throw new TypeError(`Constructor ${val===null||val===undefined?val:val.constructor.name} does not match ${type.typeConstructors}${!(val instanceof Object)&&type.useInstanceOf&&val.constructor.name===type.typeConstructors?" because of using instanceof":""}.`);
        ;
        ["ands","nots"].forEach2(t=>{
        if(type[t]){
            type[t].forEach2(x=>{
                var required=Types.predef[type.typeConstructors][t][x];
                if(Types.predef[type.typeConstructors]&&Types.predef[type.typeConstructors][t]&&required){
                    checksDone[t]=required(val);
                    if(!checksDone[t])throw new TypeError(`The value did not match the ${t.slice(0,-1)} named '${x}'.`)
                }else{
                    throw new ReferenceError(`Invalid ${t.slice(0,-1)} value '${x}' of ${type.typeConstructors}.`);
                }
            });
        }else{
            checksDone[t]=true;
        }
        
        }
        );
        propsBlock:{
            if(type.props){
                if(val===undefined||val===null)throw new TypeError(`Cannot read properties of ${val}.`);
                for(let i in type.props){
                if(i==="*"){
                    if(!Types.predef[type.typeConstructors].props["*"](val,type.props["*"])){
                        checksDone.props=false;
                        //throw new TypeError(`The property`)
                        return false;
                        //break propsBlock;
                    }else{
                        checksDone.props= true;
                        break propsBlock;
                    }
                    
                };
                if(!(i in val))throw new ReferenceError(`Property '${i}' does not exist.`);
                if(!Types.checkType(val[i],type.props[i])){
                    checksDone.props=false;
                    return false;
                    //break propsBlock;
                }
            }
            checksDone.props=true;
            }else{checksDone.props=true};
        };
        if(checksDone.all){
            return true;
        }else{
            return false;
        }
  
    
}

    function notNeeded(val){
            return val===undefined||val===""||val!==val;
    }
    //********RANGE*********
    function range(from,to,step){
        if(notNeeded(parseInt(from))||notNeeded(parseInt(to)))throw new TypeError("Argument missing");
        if(notNeeded(step))step=1;
        if(step===0)throw new RangeError("Step cannot be 0.");
        if((from>to&&step>0)||(from<to&&step<0))throw new RangeError("Invalid step value.");
        var tmp=[];
        for(let i=from;step>0?i<to:i>to;i+=step){
            tmp.push(i);
        }
        return $$(tmp);
    }
    //*******SLICING*******
    //arr[from:to:step]
    function slice(arr,from,to,step){
        if(notNeeded(step))step=1;
        if(notNeeded(from))from=step<0?-1:0;
        if(notNeeded(to))to=step<0?-arr.length-1:arr.length;    
        var final=[];
        if(from<0)from=arr.length+from;
        if(to<0)to=arr.length+to;    
        for(var i=from;step<0?i>to:i<to;i+=step) 
            if(i in arr)final.push(arr[i]);
        return $$(final);
    }
    //********PROXY FOR ARRAY*****
    if(Array.isArray(data)){
        var handler={
            get:function(x,y){
                if(typeof y==="string"&&y.toString().match(/\:/)){
                    var indices=y.toString().split(":").map(x=>parseInt(x));
        
                    return slice(x,...indices);
                }else{
                    if(y.toString().match(/^\u{2d}{0,1}[0-9]+$/u))return y<0?x[+y+x.length]:x[+y];
                    else return x[y];
                }
        
            },
            set:function(x,y,z){
                if(y.toString().match(/:/)){
                    var tmp=[];
                    var toAssign=Array.from(z);
                    for(let i=0;i<data.length;i++){
                        tmp.push(i);
                    }
                    var sliced=slice(tmp,...y.toString().split(":").map(x=>parseInt(x))); 
                
                    if(toAssign.length===sliced.length){
                        sliced.forEach((i,j)=>{
                            x[i]=toAssign[j];
                        
                        });
                    
                    }else{
                       throw new TypeError(`Trying to assign Object of length ${toAssign.length} to Array of length ${sliced.length}.`);
                    } 
                }else{
                    if(y.toString().match(/^\u{2d}{0,1}[0-9]+$/u)){
                        x[y<0?+y+x.length:+y]=z;
                    }else{
                        x[y]=z;
                    }
                }
            }
        }
        let final=new Proxy(data,handler);
        final.lockType=function(types){
            final.types=types;
        };
        return final;
    }else if(typeof data==="string" && data.match(/^[\d-]+\.\.[\d-]+/)){
        //*****RANGES **********
        //from..to..step
        return range(...data.split("..").map(x=>+x));
    }else if(typeof data==="function"){
    //*******FUNCTIONS WITH TYPES ******
    return { lockType:function(obj){
    var that=data;
    return function(...attrs){
        
        obj.argument.every((val,i)=>{
            if(val instanceof Types){
                return Types.checkType(arguments[i],val);
            }else if(!val){
                return true;
            }else{
                return Types.checkType(arguments[i],Types.parse(val));
            }
        });
        
        let output= new.target? new that(...attrs):that(...attrs);
        
        if(obj.returnValue instanceof Types){
            Types.checkType(output,obj.returnValue);
        }else if(obj.returnValue){
            Types.checkType(output,Types.parse(obj.returnValue));
        }
        
    }
    
}}
    
    }
    else throw new TypeError("Invalid value pased to $$");
}
